Change Log
==========

Just a simple journal to keep track of changes or easily, please add new changes to the top of the file.
Formating is below. Please note that the extra characters are so they are rendered properly as rst documents.

_____________________________________________________________________________

| Author: Actual name is prefered but a consistant alias is ok.
| Branch: What branch the commit is being added to.
| Date & Time: dd/mm/yyyy hh:mm
| Description: Elaborate on the commit message and possbile issue that may arise because of it.

______________________________________________________________________________

| Author: William Behrens
| Branch: master
| Date & Time: 26/3/2021 12:54
| Description: Fixed single unit entity collision and added the start of multi unit entities

| Author: William Behrens
| Branch: master
| Date & Time: 17/2/2021 11:34 pm
| Description: Fixed rst formating.

| Author: William Behrens
| Branch: master
| Date & Time: 17/2/2021 10:21 pm
| Description: DarkMagic Window now wraps NCurses WINDOW*, and Screen sorta wraps NCurses stdscr.

| Author: William Behrens
| Branch: master
| Date & Time: 16/2/2021 10:30 pm
| Description: Smoothed entity system a bit and began refining the logging system.

| Author: William Behrens
| Branch: master
| Date & Time: 15/2/2021 11:46 pm
| Description: Added the beginning of the entity system alongside the player, player movement is still rough (need to add button queueing) and entity's cannot be moved on their own as far as I can tell right now.

| Author: William Behrens
| Branch: master
| Date & Time: 13/2/2021 9:27 pm
| Description: Remade readme and change log to rst documents, remade proj file in scala, changed 'test' target to 'demo' target.

| Author: William Behrens
| Branch: master
| Date & Time: 11/2/2021 7:43 pm
| Description: Re-organized the project to make it more maintainable.
