#include "darkmagic/DarkMagic.hpp"

#include "mylog.hpp"

#include <curses.h>
#include <iostream>

using namespace darkmagic;

int main() {
	mylog::changeLogLevel(5);

	std::vector<std::string> sprite1 = {"$"};
	std::vector<std::string> sprite2 = {"@"};

	Scene MyScene;
	IEntityComponent NPC(sprite1);
	ControlableEntity Player(sprite2);

	MyScene.drawBorder('|', '|', '-', '-', '*');

	MyScene.addComponent(NPC);
	MyScene.addComponent(Player);

	do {
		// pain time
		MyScene.draw();
	} while( Player.getMove() != 'x' );

	return MyScene.end();

	/*initscr();
	refresh();

	std::cin.get();
	
	endwin();*/
}
