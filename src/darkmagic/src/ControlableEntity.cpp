#include "darkmagic/ControlableEntity.hpp"
#include "darkmagic/IEntityComponent.hpp"

#include "ncurses.h"

#include <string>

namespace darkmagic {
	ControlableEntity::ControlableEntity(std::vector<std::string>& sprite) : IEntityComponent(sprite) {}

	ControlableEntity::~ControlableEntity() {}

	int ControlableEntity::getMove(int dist) {
		int button = wgetch(this->_win);
		switch( button ) {
			case 'w':
				this->mvUP(dist);
				break;
			case 's':
				this->mvDOWN(dist);
				break;
			case 'a':
				this->mvLEFT(dist);
				break;
			case 'd':
				this->mvRIGHT(dist);
				break;
			default:
				break;
		}
		return button;
	}

} // namespace darkmagic
