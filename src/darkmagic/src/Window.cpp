#include "darkmagic/Window.hpp"

#include "mylog.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <ncurses.h>

namespace darkmagic {
	Window::Window(int _winHeight, int _winWidth, int startX, int startY) {
		_win = newwin(_winHeight, _winWidth, startX, startY);
		if( !has_colors() ) { mylog::error("Terminal doesn't support color", -1); }
	}

	Window::~Window() {
		endwin();
	}

	const void Window::close() const {
		this->~Window();
	}

	const void Window::refresh() const {
		wrefresh(this->_win);
	}

	const void Window::clear() const {
		wclear(this->_win);
	}

	const void Window::drawBorder(char leftRight, char topBottem, char corners) const {
		// box(this->_win, leftRight, topBottem);
		wborder(this->_win, leftRight, leftRight, topBottem, topBottem, corners, corners, corners, corners);
		this->refresh();
	}

	const void Window::setColors(short fgColor, short bgColor) const {
		init_pair(1, fgColor, bgColor);
		wattron(this->_win, COLOR_PAIR(1));
	}

	const int Window::getCols() {
		return getmaxx(this->_win);
	}

	const int Window::getRows() {
		return getmaxy(this->_win);
	}


	const int Window::getInput() {
		return wgetch(this->_win);
	}


	const void Window::textBox(int* y, int* x, std::string text) const {
		// mvwprintw(this->_win, y, x, text);
	}

	const bool Window::takeKeypad() const {
		if( (keypad(this->_win, true)) ) {
			std::ostringstream message;
			std::string warning = "Failed to pass keypad to _window: ";

			message << warning << this;

			mylog::warning(message.str());
			return false;
		}
		return true;
	}

	// explicit operator WINDOW*() const {
	//	return *(this->__win);
	//}

} // namespace darkmagic
