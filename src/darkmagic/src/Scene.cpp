#include <vector>
#include <iostream>

#include "ncurses.h"
#include "mylog.hpp"

#include "darkmagic/IEntityComponent.hpp"
#include "darkmagic/Scene.hpp"

namespace darkmagic {

	Scene::Scene(int scrHeight, int scrWidth, int scrStartX, int scrStartY)
		: _win(newwin(stdHeight, stdWidth, 0, 0)), _height(scrHeight), _width(scrWidth), _startx(scrStartX), _starty(scrStartY) {
		initscr();

		if( has_colors() ) {
			start_color();
		} else {

			mylog::error("Terminal doesn't support color", -1);
		}

		// TODO: work on other flags
		cbreak();
		noecho();
	}

	Scene::Scene(WINDOW* NCWindow, int scrStartX, int scrStartY)
		: _win(NCWindow), _height(getmaxy(NCWindow)), _width(getmaxx(NCWindow)), _startx(0), _starty(0) {
		initscr();

		if( has_colors() ) {
			start_color();
		} else {
			mylog::error("Terminal doesn't support color", -1);
		}

		// TODO: work on other flags
		cbreak();
		noecho();
	}

	Scene::~Scene() {
		clear();
		endwin();
		delete this->_win;
	}

	void Scene::addComponent(IEntityComponent& comp) {
		comp.addEvent(&this->_win);
		this->m_componentList.push_back(&comp);
		mylog::note("Added new component to scene");
	}
	void Scene::addMap(Map& map) {}
	void Scene::drawBorder(char leftSymbol, char rightSymbol, char topSymbol, char bottomSymbol, char cornerSymbol) {
		wborder(this->_win, leftSymbol, rightSymbol, topSymbol, bottomSymbol, cornerSymbol, cornerSymbol, cornerSymbol, cornerSymbol);
		this->draw();
	}

	void Scene::setColors(short fgColor, short bgColor) {}
	void Scene::changeColors(short fgColor, short bgColor) {}
	void Scene::removeColors() {}

	int Scene::getWidth() {
		return this->_width;
	}
	int Scene::getHeight() {
		return this->_height;
	}

	void Scene::logMap() {
		/*for(int i = 0; i < this->_map.size; i++) {
			logger::
		}*/
	}
	void Scene::printComponentList() {}
	void Scene::draw() {
		// std::cout << this->m_componentList.size() << std::endl;
		for( size_t i = 0; i < this->m_componentList.size(); i++ ) { 
            // this is having out of bounds troubles
            this->m_componentList.at(i)->draw(); 
        }
		refresh();
		wrefresh(this->_win);
	}

	int Scene::end() {
		return 1;
	}
} // namespace darkmagic
