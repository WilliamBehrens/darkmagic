#include "darkmagic/IEntityComponent.hpp"

namespace darkmagic {
	class ControlableEntity : public IEntityComponent {
		public:
		ControlableEntity(std::vector<std::string>& sprite);
		virtual ~ControlableEntity();

		virtual int getMove(int dist = 1);
	};
} // namespace darkmagic
