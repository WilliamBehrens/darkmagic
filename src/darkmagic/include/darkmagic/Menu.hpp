#pragma once

#include "darkmagic/Window.hpp"

#include <ncurses.h>

#include <string>
#include <vector>

namespace darkmagic {
	class Menu : public Window {
		public:
		Menu(int scrHeight = 0, int scrWidth = 0, int scrStartX = 0, int scrStartY = 0);
		Menu(std::vector<std::string> options, int scrHeight = 0, int scrWidth = 0, int scrStartX = 0, int scrStartY = 0);
		virtual ~Menu();

		int draw();

		// virtual void close();

		protected:
		std::vector<std::string> _options;
	};
} // namespace darkmagic
