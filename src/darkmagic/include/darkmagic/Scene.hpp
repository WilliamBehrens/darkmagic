#pragma once

#include "darkmagic/IEntityComponent.hpp"
#include "darkmagic/Map.hpp"
#include "ncurses.h"

#include <string>
#include <vector>

namespace darkmagic {
	static int stdHeight = getmaxy(stdscr);
	static int stdWidth = getmaxx(stdscr);

	class Scene {
		public:
		Scene(int scrHeight = stdHeight, int scrWidth = stdWidth, int scrStartX = 0, int scrStartY = 0);
		Scene(WINDOW* NCWindow, int scrStartX = 0, int scrStartY = 0);
		~Scene();

		void addComponent(IEntityComponent& comp);
		void addMap(Map& map);
		// void addMenu(Menu& newMenu, std::string options[]);

		void drawBorder(char leftSymbol = ' ', char rightSymbol = ' ', char topSymbol = ' ', char bottomSymbol = ' ', char cornerSymbol = ' ');

		void setColors(short fgColor, short bgColor);
		void changeColors(short fgColor, short bgColor);
		void removeColors();

		int getWidth();
		int getHeight();

		void logMap();
		void printComponentList();

		void draw();
		int end();

		operator WINDOW*() {
			return _win;
		}

		protected:
		Map m_Map;
		std::vector<IEntityComponent*> m_componentList;

		private:
		int _height, _width, _startx, _starty, _colorId;
		WINDOW* _win;
	};
} // namespace darkmagic
