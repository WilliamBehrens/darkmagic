#pragma once

#include <ncurses.h>

#include <string>

namespace darkmagic {
	class Window {
		public:
		Window(int winHeight, int winWidth, int startX = 0, int startY = 0);
		~Window();

		const void drawBorder(char leftRight, char topBottem, char corners) const;
		const void setColors(short fgColor, short bgColor) const;
		const void textBox(int* y, int* x, std::string text) const;
		const bool takeKeypad() const;

		const int getCols();
		const int getRows();
		const int getInput();

		virtual const void close() const;
		virtual const void refresh() const;
		virtual const void clear() const;

		operator WINDOW*() {
			return _win;
		}

		protected:
		int _height, _width;
		WINDOW* _win;
	};
} // namespace darkmagic
