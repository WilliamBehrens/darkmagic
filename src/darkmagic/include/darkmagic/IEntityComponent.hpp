#pragma once


#include "ncurses.h"

#include <string>
#include <vector>

namespace darkmagic {
	enum direction
	{
		up = 0,
		down,
		left,
		right
	};

	class IEntityComponent {
		public:
		IEntityComponent(std::vector<std::string> sprite) : sprite(sprite) {}
		virtual ~IEntityComponent();

		virtual void draw();

		virtual bool mvUP(int dist);
		virtual bool mvDOWN(int dist);
		virtual bool mvLEFT(int dist);
		virtual bool mvRIGHT(int dist);
		virtual bool mv(direction dir, int dist);
		virtual bool mvTo(uint x, uint y);

		virtual int getX();
		virtual int getY();

		virtual void checkBounds(); // should check and correct the x,y cords of the currenty entity
		virtual bool isTouching(IEntityComponent& other);

		void addEvent(WINDOW** myWin);

		protected:
		int x, y;
		WINDOW* _win;
		std::vector<std::string> sprite;
		// temporary patches till height can be added to entities
		std::string _s = sprite.at(0);
		std::string _e;

		private:
		IEntityComponent();
	};
} // namespace darkmagic
